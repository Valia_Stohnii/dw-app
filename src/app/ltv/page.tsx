import React from 'react';
import Header from "@/components/Header";
import HomePage from "@/components/pages/HomePage";

const LtvPage = () => {
    return (
        <div>
            <header>
                <Header/>
            </header>
            <main>
                <p>ltv</p>
            </main>
            <footer>
                <div>
                    <p>Copyright 2023 DATAWAVE | All Rights Reserved</p>
                </div>
            </footer>
        </div>
    )
}

export default LtvPage;