import Image from 'next/image'
import CustomerChart from "@/components/charts/CustomerCart";
import Header from "@/components/Header";
import Head from 'next/head';
import HomePage from "@/components/pages/HomePage";

export default function Home() {
  return (
      <div>
          <Head>
              <link rel="icon" href="/favicon.ico" />
          </Head>
          <header>
            <Header/>
          </header>
        <main>
          <HomePage/>
        </main>
          <footer>
              <div>
                  <p>Copyright 2023 DATAWAVE | All Rights Reserved</p>
              </div>
          </footer>
      </div>
  )
}
