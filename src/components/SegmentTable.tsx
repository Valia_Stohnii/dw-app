'use client'
import React from "react";
import '../styles/tables.scss'

const SegmentTable = () => {
    return (
        <div className='segment__table'>
            <table>
                <caption>
                    Segment Definitions
                </caption>
                <thead>
                    <tr>
                        <th>Segment</th>
                        <th>Cohort Description</th>
                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <td>Average Customers</td>
                        <td>This cohort bought between 374 and 990 days age, urdered one or twe times and spent more than $40</td>
                    </tr>
                    <tr>
                        <td>Best Customers</td>
                        <td>This cohort bought less than 374 days age, has arceres at least three times and spent more than 1107</td>
                    </tr>
                    <tr>
                        <td>Frequent Low Players</td>
                        <td>This cohort bought between 374 and 990 days age, urdered one or twe times and spent more than $40</td>
                    </tr>
                    <tr>
                        <td>Has Potential</td>
                        <td>This cohort bought between 374 and 990 days age, urdered one or twe times and spent more than $40</td>
                    </tr>
                    <tr>
                        <td>Lost Customers</td>
                        <td>Times and spent less than $40 This cohort bought less than 374 days ago, has ordered a wast two times</td>
                    </tr>
                    <tr>
                        <td>Lost High Spender</td>
                        <td>This cohort bought between 374 and 990 days age, urdered one or twe times and spent more than $40</td>
                    </tr>
                    <tr>
                        <td>New Customers</td>
                        <td>This cohort urdered less than 374 says ags, has spent less than $107 least once</td>
                    </tr>
                    <tr>
                        <td>New High Spender</td>
                        <td>This cohort bought between 374 and 990 days age, urdered one or twe times and spent more than $40</td>
                    </tr>
                    <tr>
                        <td>Previously Loyal</td>
                        <td>This cohort bought between 374 and 990 days age, urdered one or twe times and spent more than $40</td>
                    </tr>
                    <tr>
                        <td>Worst Customers</td>
                        <td>This short ordered more than 374 days ago has times and spent more than $40</td>
                    </tr>
                </tbody>
            </table>
        </div>
    )
}

export default SegmentTable