'use client'
import React from "react";
import '../../styles/homepage.scss'
import CustomerChart from "@/components/charts/CustomerCart";
import AverageDaysChart from "@/components/charts/AverageDaysChart";
import AverageNumberChart from "@/components/charts/AverageNumberChart";
import AveragePurchaseChart from "@/components/charts/AveragePurchaseChart";
import SegmentTable from "@/components/SegmentTable";

const HomePage = () => {
    return (
        <div className='homepage'>
            <div className='homepage__segment'>
                <div className='homepage__segment__chart'>
                    <p className='homepage__segment__chart--title'>Customer per segment</p>
                    <CustomerChart/>
                </div>
                <SegmentTable/>
            </div>
            <div className='homepage__average'>
                <ul className='homepage__average__list'>
                    <li className='homepage__average__list--item'>
                        <p className='homepage__average__list--item--title'>Average days since last purchase</p>
                        <AverageDaysChart/>
                    </li>
                    <li className='homepage__average__list--item'>
                        <p className='homepage__average__list--item--title'>Average number of purchase</p>
                        <AverageNumberChart/>
                    </li>
                    <li className='homepage__average__list--item'>
                        <p className='homepage__average__list--item--title'>Average  purchase amuont</p>
                        <AveragePurchaseChart/>
                    </li>
                </ul>
            </div>
        </div>
    );
}
export default HomePage