'use client'
import React from "react";
import Image from 'next/image';
import logoIcon from "../images/DW-logo.svg";
import ltvIcon from "../images/clock.svg";
import rfmIcon from "../images/dolar.svg";
import prdIcon from "../images/clock (2) 2 (1).svg";
import diagramIcon from "../images/diagram.svg";
import Link from "next/link";
import '../styles/header.scss';

const Header = () => {
    return (
        <div className="header">
            <Link href="/" className="header__logo">
                <Image
                    priority
                    src={logoIcon}
                    alt="DW Logo"
                    className="header__logo--img"
                />
                <p className="header__logo--text">DATAWAVE</p>
            </Link>
            <div className="header__nav">
                <ul className="nav__list">
                    <li className="nav__list__item">
                        <Image
                            priority
                            src={ltvIcon}
                            alt=""
                            className="nav__list__item--img"
                        />
                        <Link href="/ltv" className="nav__list__item--link">LTV</Link>
                    </li>
                    <li className="nav__list__item">
                        <Image
                            priority
                            src={rfmIcon}
                            alt=""
                            className="nav__list__item--img"
                        />
                        <Link href="/" className="nav__list__item--link">RFM</Link>
                    </li>
                    <li className="nav__list__item">
                        <Image
                            priority
                            src={prdIcon}
                            alt=""
                            className="nav__list__item--img"
                        />
                        <Link href="/" className="nav__list__item--link">Products associations</Link>
                    </li>
                    <li className="nav__list__item">
                        <Image
                            priority
                            src={diagramIcon}
                            alt=""
                            className="nav__list__item--img"
                        />
                        <Link href="/" className="nav__list__item--link">Acquisition comparison</Link>
                    </li>
                </ul>
            </div>
            <input type="date" className="header__date" defaultValue='2023-10-19'/>
        </div>
    );
}

export default Header