'use client'
import React from 'react';
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';
import { Bar } from 'react-chartjs-2';
import faker from 'faker';
import '../../styles/charts.scss'

ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend
);

export const options = {
    indexAxis: 'y' as const,
    elements: {
        bar: {
            borderWidth: 2,
        },
    },
    responsive: true,
    plugins: {
        legend: {
            display: false
        },
        title: {
            display: false,
            text: 'Average  purchase amount',
        },
    }
};

const labels = ['Average Customers', 'Best Customers', 'Frequent Low Players', 'Has Potential', 'Lost Customers', 'Lost High Spender', 'New Customers', 'New High Spender', 'Previously Loyal', 'Worst Customers'];

export const data = {
    labels,
    datasets: [
        {
            data: labels.map(() => faker.datatype.number({ min: 0, max: 180 })),
            backgroundColor: [
                '#FFFFD9',
                '#DCF0C8',
                '#C7E9B4',
                '#7FCDBB',
                '#41B6C4',
                '#1D91C0',
                '#225EA8',
                '#253494',
                '#193481',
                '#081D58'
            ],
            borderColor: 'transparent',
            borderRadius: 5,
        }
    ],
};

const AveragePurchaseChart = () => {
    return <Bar className='average-purchase-chart' options={options} data={data} />;
}

export default AveragePurchaseChart